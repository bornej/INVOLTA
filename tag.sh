#!/bin/sh
jsontag_v2_dir=./tools
flacs=./flacs
tags=./tags
for source in $tags/*
do
    # get the file "source" prefix
    source_basename=$(echo "$source" | sed -r "s/.+\/(.+)\..+/\1/")
	metaflac --remove-all $flacs/$source_basename.flac
    ruby $jsontag_v2_dir/jsontag_v2.rb $source $flacs/$source_basename.flac
done
